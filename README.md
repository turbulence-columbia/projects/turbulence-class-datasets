# Turbulence Theory and Modeling Datasets

## Overview 

This repository contains datasets of turbulent flows to be used by students of the Turbulence Theory and Modeling class.
If you experience any problem (or spot any typo/mistake), please report it to `mg3929@columbia.edu`.

## DATASET $`\#1`$ Direct Numerical Simulation of Open Channel Flow at $`Re_\tau=250`$

The incompressible Navier-Stokes equations are considered in a Cartesian reference system,

$`\nabla \cdot \textbf{u} = 0 \ , `$ 

$` \frac{\partial \textbf{u}}{\partial t} +  \textbf{u} \cdot  \nabla \textbf{u} = - \frac{1}{\rho}\nabla p - \nabla \cdot \boldsymbol{\tau} - \frac{1}{\rho} \nabla P \ , `$

where $`\textbf{u} = (u_1,u_2,u_3)`$ (or, equivalently, $`\textbf{u} = (u,v,w)`$) is the velocity field along the streamwise $`(x_1)`$, spanwise $`(x_2)`$, and vertical $`(x_3)`$ coordinate directions, $`t`$ is the time, $`\rho`$ is the (constant) fluid density, $`p`$ is the pressure term, $`\boldsymbol{\tau}`$ is the viscous-stress tensor. 
The term $`- \frac{1}{\rho} \nabla P`$ is a constant kinematic pressure gradient driving the flow, here assumed to be constant and uniform.
The viscous tensor is $`\boldsymbol{\tau}=-2\nu \textbf{S}`$, where $`\nu = \mathrm{const}`$ is the kinematic viscosity of the Newtonian fluid and $`\textbf{S}`$ is the rate of strain tensor.

<img src="figure/geometry_openchannel.png"  width="600" height="260"> 

_Figure 1 Schematic diagram of the computational domain._

A single numerical experiments is carried out in the open channel flow setup (similar to Moser et al., 1999), solving the incompressible Navier-Stokes equations in a domain $`[0,L_{1}] \times [0,L_{2}] \times [0,L_{3}]`$, where $`L_{1} = 2 \pi h`$, $`L_{2} = \frac{4}{3} \pi h`$, $`L_{3} = h`$ and where $`h=1000 \ \mathrm{m}`$ denotes the height of the open channel. 
Free slip (or symmetry) boundary conditions are imposed at the top of the computational domain ($`\partial u_1/\partial x_3 = \partial u_2/\partial x_3 = u_3 = 0`$), no-slip applies at the lower surface ($`u_i=0`$), and periodic boundary conditions are enforced along each side (aka, lateral boundaries).
A kinematic pressure gradient term $`- \frac{1}{\rho} \partial P / \partial x_1 = 1 \ \mathrm{m}/\mathrm{s}^2`$ drives the flow along the $`x_1`$ coordinate direction, yielding $`u_{\tau} = \sqrt{\tau_w/\rho} = 1 \ \mathrm{m}/\mathrm{s}`$, where $`\tau_w`$ is the mean kinematic stress at the lower surface.
The kinematic viscosity is set to $`4.0 \ \mathrm{m}^2/\mathrm{s}`$, resulting in $`Re_{\tau} = \frac{u_\tau h}{\nu} = 250`$.

The computational domain is discretized using $`N_1 \times N_2 \times N_3 = 128\times 128 \times 288`$ collocation nodes in the $`x_1, x_2, x_3`$ directions, respectively. 
Time integration is $`T = 40 H/u_\tau = 4 \times 10^4 \ \mathrm{s}`$. 
Specifically, simulations are run for $`8 \times 10^5`$ steps with $`\Delta t = 5 \times 10^{-2} \, \mathrm{s}`$.
An instantaneous snapshot of the three-dimensional velocity and pressure fields are saved every $`5000`$ steps (corresponding to am output time-step of $`T_{out} = 250 \ \mathrm{s}`$) in the [.cbd](https://gitlab.com/turbulence-columbia/codes/cbd-format) Cartesian Binary Data format. 

**Details of the DNS run**

| $`L_1 \times L_2 \times L_3  \ \mathrm{[m^3]}`$ | $`N_1 \times N_2 \times N_3`$ | $`Dt \ \mathrm{[s]}`$ | $`Nt`$ | $`T \ \mathrm{[s]}`$ | $`T_{out} \ \mathrm{[s]}`$ | $`u_\tau \ \mathrm{[m/s]}`$ |
|-------|------------------------|------------------------|----|----|---|--|
| $`2\pi h \times \frac{4}{3} \pi h \times h , h = 10^3 \ \mathrm{m}`$ | $`128 \times 128 \times 288`$ |  $`5\times 10^{-2}`$  |  $`8 \times 10^5`$  |  $`5 \times 10^4`$ | $`250`$ |   $`1`$  | 

**The dataset can be downloaded from [here](https://drive.google.com/open?id=1bvEyQ9oUJQao1HH0Bdh-LsTgwtXRO8ob) ($`12.6`$ GB of data!).**

**Please use the scripts from the [.cbd](https://gitlab.com/turbulence-columbia/codes/cbd-format) repository to load the dataset.**

**The instantaneous snapshots of the 3-D velocity components and pressure are in `.cbd` format and are located in the `channel_flow_retau=250_2piHx1p33piHxH_H=1000/output/instantaneous-fields/` folder.** 

**The naming convention for each scalar field 3-D snapshot is `scalarField-ostep.cbd` where `scalarField` is either `u`, `v`, `w`, or `p`, and `ostep` is an integer denoting the output step. For example, `u-000.cbd` will denote the $`u`$ component of the velocity vector field at the first output step.**    

## DATASET $`\#2`$ Direct Numerical Simulation of Open Channel Flow at $`Re_\tau=500`$

Same equations and computational domain as those of DATASET $`\#1`$.
A pressure gradient term $`- \frac{1}{\rho} \partial P / \partial x_1 = 1 \ \mathrm{m}/\mathrm{s}^2`$ drives the flow along the $`x_1`$ coordinate direction, yielding $`u_{\tau} = \sqrt{\tau_w/\rho} = 1 \ \mathrm{m}/\mathrm{s}`$, where $`\tau_w`$ is the mean stress at the lower surface.
The kinematic viscosity is set to $`2.0 \ \mathrm{m}^2/\mathrm{s}`$, resulting in $`Re_{\tau} = \frac{u_\tau h}{\nu} = 500`$.

The computational domain is discretized using $`N_1 \times N_2 \times N_3 = 256\times 256 \times 578`$ collocation nodes in the $`x_1, x_2, x_3`$ directions, respectively. 
Time integration is $`T = 7.5 H/u_\tau = 7.5 \times 10^3 \ \mathrm{s}`$. 
Specifically, simulations are run for $`3\times 10^5`$ steps with $`\Delta t = 0.025 \, \mathrm{s}`$.
An instantaneous snapshot of the three-dimensional velocity and pressure fields are saved every $`5000`$ steps (corresponding to am output time-step of $`T_{out} = 125 \ \mathrm{s}`$) in the [.cbd](https://gitlab.com/turbulence-columbia/codes/cbd-format) Cartesian Binary Data format (see reference section below). 

**Details of the DNS run**

| $`L_1 \times L_2 \times L_3  \ \mathrm{[m^3]}`$ | $`N_1 \times N_2 \times N_3`$ | $`Dt \ \mathrm{[s]}`$ | $`Nt`$ | $`T \ \mathrm{[s]}`$ | $`T_{out} \ \mathrm{[s]}`$ | $`u_\tau \ \mathrm{[m/s]}`$ |
|-------|------------------------|------------------------|----|----|---|--|
| $`2\pi h \times \frac{4}{3} \pi h \times h , h = 1000 \ \mathrm{m}`$ | $`256 \times 256 \times 576`$ |  $`2.5 \times 10^{-2}`$  |  $`3 \times 10^5`$  |  $`7.5 \times 10^3`$ | $`125`$ |   $`1`$  | 

**The dataset can be downloaded from [here](https://drive.google.com/drive/folders/1L8dRL_9T-5vEiZ1-Mp5jvDxJJepRKIS0?usp=sharing) ($`37`$ GB of data!).**

**Please use the scripts from the [.cbd](https://gitlab.com/turbulence-columbia/codes/cbd-format) repository to load the dataset.**

**The instantaneous snapshots of the 3-D velocity components and pressure are in `.cbd` format and are located in the `channel_flow_retau=500_2piHx1p33piHxH_H=1000/output/instantaneous-fields/` folder.** 

**The naming convention for each scalar field 3-D snapshot is `scalarField-ostep.cbd` where `scalarField` is either `u`, `v`, `w`, or `p`, and `ostep` is an integer denoting the output step. For example, `u-000.cbd` will denote the $`u`$ component of the velocity vector field at the first output step.**    



## References
1. R.D. Moser, J. Kim, N.N. Mansour. Direct numerical simulation of turbulent channel flow up to $`Re_\tau= 590`$. [Phys. Fluids, 1999, 11, 943-945](https://doi.org/10.1063/1.869966)
